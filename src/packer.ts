/*tslint:disable:no-console*/

/**
 * @galeanne-thorn/md-packer
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import * as colors from "colors";
import * as fs from "fs";
import * as gm from "gray-matter";
import * as yaml from "js-yaml";
import * as path from "path";

// Note the gray-matter.d.ts does not define sections but it is possible option
const grayMatterOptions: gm.GrayMatterOption<string, {}> = { sections: true }; // as gm.GrayMatterOption<string, {}>;

/**
 * Compiles gray-matter style markdown files from the input folder into one json file
 *
 * @param inputFolder - Folder with input files
 * @param outputFile - Name of the output file
 */
export default function pack(inputFolder: string, outputFile: string = "game.json"): number {

    if (!isValidFolder(inputFolder)) {
        return 1;
    }

    console.info(`Reading input folder ${inputFolder}`);
    const result = process(inputFolder);

    console.info(`Writing output file ${outputFile}`);
    fs.writeFileSync(outputFile, JSON.stringify(result, null, 2));

    console.info(`Done`);

    return 0;
}

/**
 * Validates the folder
 *
 * @param folder Path to the folder to validate
 */
function isValidFolder(folder: string): boolean {
    if (!fs.existsSync(folder)) {
        console.error(colors.red(`Folder '${folder}' does not exist`));
        return false;
    }
    const stat = fs.statSync(folder);
    if (!stat.isDirectory) {
        console.error(colors.red(`'${folder}' is not folder`));
        return false;
    }
    return true;
}

/**
 * Processes folder
 * @param folder - Input folder to process
 */
function process(folder: string): object {

    const dirContent = fs.readdirSync(folder, { withFileTypes: true });
    const result = {};

    dirContent.forEach(e => {
        if (e.isDirectory()) {
            console.debug(`Processing folder ${e.name}`);
            result[e.name] = process(path.join(folder, e.name)); // Recursion
        }
        else if (e.isFile()) {
            const ext = path.extname(e.name);
            const name = path.basename(e.name, ext);
            const fullPath = path.join(folder, e.name);
            let data: object = null;
            switch (ext) {
                case ".md":
                    console.debug(`Processing file ${e.name}`);
                    data = processMarkdownFile(fullPath);
                    break;

                case ".json":
                    console.debug(`Processing file ${e.name}`);
                    data = processJsonFile(fullPath)
                    break;

                case ".yaml":
                case ".yml":
                    console.debug(`Processing file ${e.name}`);
                    data = processYamlFile(fullPath)
                    break;

                default:
                    console.debug(`Skipping ${e.name}`);
                    return;
            }
            result[name] = data;
        }
    });

    return result;
}

/**
 * Converts the front matter, sections and markdown into gemini game template
 * @param fileName
 */
function processMarkdownFile(fileName: string): object {
    const file = gm.read(fileName, grayMatterOptions);
    // Get front matter data or empty object if file is pure Markdown
    const data = file.data ?? {};
    // Add UI section to data if not present
    if (!data.ui) {
        data.ui = {};
    }
    // Set markdown text to UI
    data.ui.text = file.content;
    // If there are sections, process them
    if (file?.sections?.length > 0) {
        if (!data.sections) {
            data.sections = {};
        }
        data.ui.sections = {};
        file.sections.forEach(s => {
            data.ui.sections[s.key] = s.content;
            data.sections[s.key] = s.data;
        });
    }
    return data;
}

/**
 * Reads JSON file
 * @param filename
 */
function processJsonFile(filename: string): object {
    var content = fs.readFileSync(filename, "utf8");
    var data = JSON.parse(content);
    return data;
}

/**
 * Reads YAML file
 * @param filename
 */
function processYamlFile(filename: string): object {
    var content = fs.readFileSync(filename, "utf8");
    var data = yaml.safeLoad(content);
    return data;
}
