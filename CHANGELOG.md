# 1.1.1 - Fix processing of pure Markdown files

Markdown files without front matter no longer crash the packer

# 1.1.0 - Add JSON and YAML support

Packs also JSON and YAML files.

Supported extensions are:
* `.md` - Markdown with front matter
* `.json` - JSON
* `.yaml`, `.yml` - YAML

Removed requirement for the Markdown file to have fields `type` and `id`

# 1.0.1 - Fix package installation

Fixed path to executable script

# 1.0.0 - Initial release

Initial release