# Gemini Game Engine - MD to JSON Packer

Packs Markdown/CommonMark `.md` files with front matter definitions,
together with JSON `.json` and YAML `.yaml` files
into one single JSON file.

The markdown part of the

## Installation

```
npm install @gemini-game-engine/md-packer
```

## Usage

### CLI

>npx @gemini-game-engine/md-packer \<input-folder\> [<output.json>]

## Example

Assuming `game` folder has following structure:

```
+-+- game
  |- info.yaml
  |-+ rooms
  | |- kitchen.md
  | \- hallway.md
  |-+ scenes
    \- data.json

```

and content of `kitchen.md` is
```
---
type: room
id: kitchen
data:
  - 1
  - 2
---
Text for kitchen
```

and content of `hallway.md` is
```
---
type: room
id: hallway
data:
  - 3
---
Text for hallway
```

running the `md-packer game game.json` generates `game.json` file with following content:

``` JSON
{
  "info": {...},
  "rooms": {
    "kitchen": {
      "type": "room",
      "id": "kitchen",
      "data": [1 ,2],
      "ui": {
        "text": "Text for kitchen"
      }
    },
    "hallway": {
      "type": "room",
      "id": "hallway",
      "data": [3],
      "ui": {
        "text": "Text for hallway"
      }
    }
  },
  "scenes": {
    "data": {...}
  }
}
```